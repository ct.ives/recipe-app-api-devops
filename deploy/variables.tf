variable "prefix" {
  default = "its"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "ct.ives@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS Postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "192936308227.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:lastest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "192936308227.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "ivestechsolutions.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}